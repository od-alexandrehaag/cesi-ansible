# Ansible

## Install

### Vagrant

```
vagrant up
```

### Ansible

```
vagrant ssh ansible
cd /ansible
ansible-galaxy install -r requirements.yml
```

## Usages

### Init ENV

```
vagrant ssh ansible
cd /ansible
ansible-playbook -i inventories/prod playbook/install.yml
ansible-playbook -i inventories/prod playbook/deploy.yml
```

- Prod :
    - http://192.168.11.11
    - http://example.local
    - http://www.example.local
    - http://app.example.local
- Preprod :
    - http://192.168.11.12
    - http://preprod.example.local
    - http://www.preprod.example.local
    - http://app.preprod.example.local

#### Tips 

Add this line in /etc/hosts :

```
192.168.11.11 preprod.example.local www.preprod.example.local app.preprod.example.local
192.168.11.12 example.local www.example.local app.example.local
```

### Monitoring

```
vagrant ssh ansible
cd /ansible
ansible-playbook -i inventories/prod playbook/monitoring.yml
```
- node exporter : http://192.168.11.11:9100/metrics
- prometheus : http://192.168.11.11:9090/graph
- grafana : http://192.168.11.11:3000

## Uninstall

```
vagrant destroy -f
```
