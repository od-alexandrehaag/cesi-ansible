# Mysql

## Vars

```yaml
mysql_root_password: "toor"
mysql_users:
  - name: test
    password: test
    state: present
    tables:
      - test
      - insane
```