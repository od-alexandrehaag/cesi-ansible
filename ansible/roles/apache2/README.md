# Apache 2

## Vars

```yaml
apache2_vhosts:
  - host: example.local
    port: 80
    alias:
      - www.example.local
    base_dir: /var/www/example.local
    document_root: /var/www/example.local
    directory_index: index.php
    create_index: true
```
 